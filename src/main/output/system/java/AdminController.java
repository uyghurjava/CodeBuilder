package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Admin;
import com.zzwtec.community.model.AdminM;
import com.zzwtec.community.model.AdminMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdminServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Admin控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdminController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_ADMIN)
	public void list(){				
		//AdminServicePrx prx = IceServiceUtil.getService(AdminServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/admin_list.html");
	}
	
	/**
	 * 添加Admin
	 */
	@ActionKey(UrlConstants.URL_ADMIN_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Admin成功");;
		try{
			//AdminServicePrx prx = IceServiceUtil.getService(AdminServicePrx.class);
			Admin model = getBean(Admin.class);		
			//prx.addAdmin(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Admin失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Admin
	 */
	@ActionKey(UrlConstants.URL_ADMIN_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Admin成功");
		try{
			String ids = getPara("ids");		
			//AdminServicePrx prx = IceServiceUtil.getService(AdminServicePrx.class);
			//prx.delAdminByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Admin失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_ADMIN_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Admin entity = new Admin();	
		setAttr("admin", entity);
		render("/system/view/admin_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_ADMIN_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdminServicePrx prx = IceServiceUtil.getService(AdminServicePrx.class);
			//AdminM modele = prx.inspectAdmin(id);
			Admin entity = new Admin();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("admin", entity);			
			render("/system/view/admin_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Admin失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Admin
	 */
	@ActionKey(UrlConstants.URL_ADMIN_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Admin成功");
		try{
			//AdminServicePrx prx = IceServiceUtil.getService(AdminServicePrx.class);
			Admin model = getBean(Admin.class);
			//prx.alterAdmin(model);
		}catch(Exception e){
			repJson = new DataObject("更新Admin失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdminServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdminMPage viewModel = prx.getAdminList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Admin[] aray = new Admin[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Admin();
			aray[i].id = "id-"+i;			
		}	
		List<Admin> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
