package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.UserSet;
import com.zzwtec.community.model.UserSetM;
import com.zzwtec.community.model.UserSetMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.UserSetServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * UserSet控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class UserSetController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_USER_SET)
	public void list(){				
		//UserSetServicePrx prx = IceServiceUtil.getService(UserSetServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/user_set_list.html");
	}
	
	/**
	 * 添加UserSet
	 */
	@ActionKey(UrlConstants.URL_USER_SET_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加UserSet成功");;
		try{
			//UserSetServicePrx prx = IceServiceUtil.getService(UserSetServicePrx.class);
			UserSet model = getBean(UserSet.class);		
			//prx.addUserSet(model);				
		}catch(Exception e){
			repJson = new DataObject("添加UserSet失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除UserSet
	 */
	@ActionKey(UrlConstants.URL_USER_SET_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除UserSet成功");
		try{
			String ids = getPara("ids");		
			//UserSetServicePrx prx = IceServiceUtil.getService(UserSetServicePrx.class);
			//prx.delUserSetByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除UserSet失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_USER_SET_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		UserSet entity = new UserSet();	
		setAttr("userSet", entity);
		render("/system/view/user_set_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_USER_SET_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//UserSetServicePrx prx = IceServiceUtil.getService(UserSetServicePrx.class);
			//UserSetM modele = prx.inspectUserSet(id);
			UserSet entity = new UserSet();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("userSet", entity);			
			render("/system/view/user_set_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取UserSet失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改UserSet
	 */
	@ActionKey(UrlConstants.URL_USER_SET_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新UserSet成功");
		try{
			//UserSetServicePrx prx = IceServiceUtil.getService(UserSetServicePrx.class);
			UserSet model = getBean(UserSet.class);
			//prx.alterUserSet(model);
		}catch(Exception e){
			repJson = new DataObject("更新UserSet失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(UserSetServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//UserSetMPage viewModel = prx.getUserSetList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		UserSet[] aray = new UserSet[12];		
		for(int i=0;i<12;i++){
			aray[i] = new UserSet();
			aray[i].id = "id-"+i;			
		}	
		List<UserSet> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
