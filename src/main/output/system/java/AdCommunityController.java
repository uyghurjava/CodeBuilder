package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.AdCommunity;
import com.zzwtec.community.model.AdCommunityM;
import com.zzwtec.community.model.AdCommunityMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdCommunityServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * AdCommunity控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdCommunityController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_AD_COMMUNITY)
	public void list(){				
		//AdCommunityServicePrx prx = IceServiceUtil.getService(AdCommunityServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/ad_community_list.html");
	}
	
	/**
	 * 添加AdCommunity
	 */
	@ActionKey(UrlConstants.URL_AD_COMMUNITY_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加AdCommunity成功");;
		try{
			//AdCommunityServicePrx prx = IceServiceUtil.getService(AdCommunityServicePrx.class);
			AdCommunity model = getBean(AdCommunity.class);		
			//prx.addAdCommunity(model);				
		}catch(Exception e){
			repJson = new DataObject("添加AdCommunity失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除AdCommunity
	 */
	@ActionKey(UrlConstants.URL_AD_COMMUNITY_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除AdCommunity成功");
		try{
			String ids = getPara("ids");		
			//AdCommunityServicePrx prx = IceServiceUtil.getService(AdCommunityServicePrx.class);
			//prx.delAdCommunityByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除AdCommunity失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_AD_COMMUNITY_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		AdCommunity entity = new AdCommunity();	
		setAttr("adCommunity", entity);
		render("/system/view/ad_community_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_AD_COMMUNITY_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdCommunityServicePrx prx = IceServiceUtil.getService(AdCommunityServicePrx.class);
			//AdCommunityM modele = prx.inspectAdCommunity(id);
			AdCommunity entity = new AdCommunity();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("adCommunity", entity);			
			render("/system/view/ad_community_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取AdCommunity失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改AdCommunity
	 */
	@ActionKey(UrlConstants.URL_AD_COMMUNITY_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新AdCommunity成功");
		try{
			//AdCommunityServicePrx prx = IceServiceUtil.getService(AdCommunityServicePrx.class);
			AdCommunity model = getBean(AdCommunity.class);
			//prx.alterAdCommunity(model);
		}catch(Exception e){
			repJson = new DataObject("更新AdCommunity失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdCommunityServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdCommunityMPage viewModel = prx.getAdCommunityList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		AdCommunity[] aray = new AdCommunity[12];		
		for(int i=0;i<12;i++){
			aray[i] = new AdCommunity();
			aray[i].id = "id-"+i;			
		}	
		List<AdCommunity> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
