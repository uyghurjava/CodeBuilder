package com.zzwtel.autocode.template.constants;

/**
 * 模型包名
 * @author yangtonggan
 * @date 2016-3-7
 */
public class ModelPackage {
	public static final String packageName = "com.pointercn.community.model";
}
