package com.zzwtel.autocode.template.model;

/**
 * 模板模型
 * @author yangtonggan
 *
 */
public abstract class TemplateModel {
	//表名
	private Table table;		
	public Table getTable() {
		return table;
	}
	public void setTable(Table table) {
		this.table = table;
	}
	
}
