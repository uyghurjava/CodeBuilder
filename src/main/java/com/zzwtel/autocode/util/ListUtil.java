package com.zzwtel.autocode.util;

import java.util.ArrayList;
import java.util.List;

/**
 * list工具类
 * @author yangtonggan
 * @date 2016-3-10
 */
public class ListUtil {
	/**
	 * 拆分list
	 * @param targe
	 * @param size
	 * @return
	 */
	public static <T> List<List<T>>  split2List(List<T> targe) {  	
		List<List<T>> listArr = new ArrayList<List<T>>();  
		int size = 0;
		if(targe.size()%2 == 1){
			size = targe.size()+1;
		}else{
			size = targe.size();
		}
		int pos = size/2;
		if(pos == 0){
			return null;
		}
		List<T>  sub1 = new ArrayList<T>();  
		List<T>  sub2 = new ArrayList<T>();  
		for(int i=0;i<targe.size();i++){
			 if(i<pos){				
				 sub1.add(targe.get(i));
			 }else{				 
				 sub2.add(targe.get(i));
			 }			 
		}
		listArr.add(sub1);
		listArr.add(sub2);
		
		return listArr;
    }  
 
}
