package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.ListHtmlTemplate;
import com.zzwtel.autocode.template.model.UIModel;

/**
 * html列表构造器
 * @author yangtonggan
 *
 */
public class ListHtmlBuilder {
	public void makeCode(UIModel vm,String modularDir){
		ListHtmlTemplate template = new ListHtmlTemplate();
		template.generate(vm, modularDir);
	}
}
