package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.EditFormHtmlTemplate;
import com.zzwtel.autocode.template.model.UIModel;

/**
 * 编辑表单构造器
 * @author yangtonggan
 * @date 2016-3-7
 */
public class EditFormHtmlBuilder {

	public void makeCode(UIModel vm,String modularDir) {
		EditFormHtmlTemplate template = new EditFormHtmlTemplate();
		template.generate(vm, modularDir);
		
	}

}
