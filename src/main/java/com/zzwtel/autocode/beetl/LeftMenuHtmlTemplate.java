package com.zzwtel.autocode.beetl;

import java.util.List;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.FileResourceLoader;
import com.zzwtel.autocode.template.constants.TemplatePath;
import com.zzwtel.autocode.template.model.FirstLevelMenuModel;
import com.zzwtel.autocode.template.model.LeftMenuModel;
import com.zzwtel.autocode.util.FileUtil;
import com.zzwtel.autocode.util.PathUtil;

/**
 * 左菜单对应的html文件
 * @author yangtonggan
 * @date 2016-6-8
 */
public class LeftMenuHtmlTemplate {
	public void generate(LeftMenuModel menuModel,String modularDir){
		try{			
			String root = PathUtil.getTemplateRoot();
			FileResourceLoader resourceLoader = new FileResourceLoader(root,"utf-8");
			Configuration cfg = Configuration.defaultConfiguration();
			GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
			//Template t = gt.getTemplate("/left-menu/left_menu.html");
			Template t = gt.getTemplate(TemplatePath.LEFT_MENU_HTML_TEMPLATE);	
			//String model = vm.getTable().getModel();
			List<FirstLevelMenuModel> firstLevelMenuList = menuModel.getChildrenMenu();		
			//绑定菜单
			t.binding("menus", firstLevelMenuList);				
			String data = t.render();			
			String out = PathUtil.getOutRoot();			
			String fileName = TemplatePath.LEFT_MENU_HTML_TEMPLATE;		
			FileUtil.write(out+modularDir+fileName, data);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
}
