package com.zzwtel.autocode.beetl;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.FileResourceLoader;

import com.zzwtel.autocode.template.constants.TemplatePath;
import com.zzwtel.autocode.template.model.ControllerModel;
import com.zzwtel.autocode.template.model.Table;
import com.zzwtel.autocode.util.FileUtil;
import com.zzwtel.autocode.util.HumpUtil;
import com.zzwtel.autocode.util.PathUtil;
import com.zzwtel.autocode.util.StrKit;


/**
 * 负责处理java模板，被构造器调用
 * @author yangtonggan
 * @date 2016-3-8
 * 
 */
public class JavaFileTemplate {
	/**
	 * 生成java代码
	 * @param m 数据模型
	 * @param 文件输出路径
	 * 需要分清 首字母 大写 ，小写 ，全部大写 ，小写
	 */
	public void generate(ControllerModel m,String dir){
		try{			
			String root = PathUtil.getTemplateRoot();
			FileResourceLoader resourceLoader = new FileResourceLoader(root,"utf-8");
			Configuration cfg = Configuration.defaultConfiguration();
			GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);		
			//Template t = gt.getTemplate("/java/@{EntityModel}Controller.java");
			Template t = gt.getTemplate(TemplatePath.JAVA_FILE_TEMPLATE);
			String model = m.getTable().getModel();			
			//首字母小写，驼峰命名
			t.binding("entityModel", model);
			//首字母大写，驼峰命名
			t.binding("EntityModel", StrKit.firstCharToUpperCase(model));
			//全部大写，下划线分割
			String _model = HumpUtil.underscoreName(model);
			t.binding("ENTITY_MODEL", _model);
			//全部小写，下划线分割			
			t.binding("entity_model", HumpUtil.toLowerCase(_model));	
			
			String data = t.render();			
			String out = PathUtil.getOutRoot();			
			String fileName = StringTemplate.getFileName(TemplatePath.JAVA_FILE_TEMPLATE, "EntityModel",StrKit.firstCharToUpperCase(model));		
			FileUtil.write(out+dir+fileName, data);
			
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args){
		JavaFileTemplate jft = new JavaFileTemplate();
		ControllerModel m = new ControllerModel();
		m.setTable(new Table());
		m.getTable().setModel("area");
		jft.generate(m, null);
	}
	
}
