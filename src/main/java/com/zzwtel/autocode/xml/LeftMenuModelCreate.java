package com.zzwtel.autocode.xml;

import java.util.List;

import com.zzwtel.autocode.db.DBUtil;
import com.zzwtel.autocode.template.model.Table;
import com.zzwtel.autocode.util.FileUtil;
import com.zzwtel.autocode.util.HumpUtil;
import com.zzwtel.autocode.util.PathUtil;

public class LeftMenuModelCreate {
    /**
     * <?xml version='1.0' encoding='UTF-8'?>
	 * <first_level_menu name="一级菜单">
	 * 		<second_level_menu id="menu-community-page" name="小区基本信息" />			
	 * </first_level_menu>
     */
	public void create() {
		System.out.println("正在生成菜单模型");	
		System.out.println("......");
		List<Table> tables = DBUtil.getAllTables();
		StringBuilder xml = new StringBuilder();
		xml.append("<?xml version='1.0' encoding='UTF-8'?>\n");
		xml.append("<left-menu>\n");		
		xml.append("	<first_level_menu name=\"一级菜单\">\n");
		for(Table t : tables){
			String model = t.getModel();
			xml.append("		<second_level_menu id=\"menu-"+t.getName()+"-page\" model=\""+model+"\" name=\""+t.getName()+"\" />\n");
		}		
		xml.append("	</first_level_menu>\n");
		xml.append("	<first_level_menu name=\"一级菜单\">\n");
		xml.append("		<second_level_menu id=\"menu-xxxx-page\" name=\"xxxx\" />\n");
		xml.append("	</first_level_menu>\n");
		xml.append("</left-menu>\n");
		//输出到文件
		String root = PathUtil.getModelRoot();
		FileUtil.write(root+"menu/left-menu-model.xml", xml.toString());		
		System.out.println("请配置"+root+"menu/left-menu-model.xml文件");		
		System.out.println("菜单模型生成成功->[请手工对菜单模型进行调整]->生成菜单");
	}
	
	public static void main(String[] args){
		LeftMenuModelCreate menu = new LeftMenuModelCreate();
		menu.create();
	}

}
